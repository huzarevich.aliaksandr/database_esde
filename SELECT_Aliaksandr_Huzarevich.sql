-- 1 task

WITH YearlyPayments AS (
  SELECT
    staff_id,
    COALESCE(SUM(amount), 0) AS total_amount
  FROM payment
  WHERE EXTRACT(YEAR FROM payment_date) = 2017
  GROUP BY staff_id
)

SELECT
  s.staff_id,
  s.first_name,
  s.last_name,
  COALESCE(yp.total_amount, 0) AS amount
FROM staff s
LEFT JOIN YearlyPayments yp ON s.staff_id = yp.staff_id
ORDER BY amount DESC;

-- 2 task
WITH RentalCounts AS (
  SELECT
    i.film_id,
    COUNT(r.inventory_id) AS film_count
  FROM inventory i
  LEFT JOIN rental r ON i.inventory_id = r.inventory_id
  GROUP BY i.film_id
)

SELECT
  f.film_id,
  COALESCE(rc.film_count, 0) AS film_count,
  f.rating
FROM film f
LEFT JOIN RentalCounts rc ON f.film_id = rc.film_id
ORDER BY film_count DESC;

-- 3 task

SELECT
  max_release_year,
  actor_id
FROM (
  SELECT
    MAX(f.release_year) AS max_release_year,
    fa.actor_id
  FROM film_actor fa
  INNER JOIN film f ON fa.film_id = f.film_id
  GROUP BY fa.actor_id
) AS actor_max_release
ORDER BY max_release_year DESC;

