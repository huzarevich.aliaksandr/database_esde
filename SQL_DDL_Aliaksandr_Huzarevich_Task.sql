-- Create the Climber table
CREATE TABLE climber (
  id bigint PRIMARY KEY,
  name varchar NOT NULL,
  address text,
  age bigint,
  experience bigint NOT NULL
);

-- Create the Country table
CREATE TABLE country (
  name varchar PRIMARY KEY,
  rating bigint
);

CREATE TABLE area (
  name varchar(100) PRIMARY KEY,
  coordinates text
);

-- Create the Mountain table
CREATE TABLE mountain (
  name varchar PRIMARY KEY,
  height bigint NOT NULL,
  area_id varchar(100) NOT NULL,
  CONSTRAINT fk_area FOREIGN KEY (area_id) REFERENCES area(name)
);

-- Create the Way table
CREATE TABLE way (
  id bigint PRIMARY KEY,
  distance bigint NOT NULL,
  difficulty varchar NOT NULL,
  experience_level varchar(100),
  mountain_id varchar,
  CONSTRAINT fk_mountain FOREIGN KEY (mountain_id) REFERENCES mountain(name)
);

-- Create the Climb table
CREATE TABLE climb (
  id bigint PRIMARY KEY,
  way_id bigint NOT NULL,
  start_date date NOT NULL,
  end_date date NOT NULL,
  weather_conditions text NOT NULL,
  CONSTRAINT fk_way FOREIGN KEY (way_id) REFERENCES way(id)
);

-- Create the Country_Area table
CREATE TABLE country_area (
  id bigint PRIMARY KEY,
  area_id varchar(100),
  country_id varchar(100),
  CONSTRAINT fk_area FOREIGN KEY (area_id) REFERENCES area(name),
  CONSTRAINT fk_country FOREIGN KEY (country_id) REFERENCES country(name)
);

-- Table: climber_emergency_case
CREATE TABLE climber_emergency_case (
  id bigint PRIMARY KEY,
  climber_id bigint NOT NULL,
  phone_number bigint NOT NULL,
  email varchar(300) NOT NULL,
  injuries text NOT NULL,
  blood_type text NOT NULL,
  climb_id bigint NOT NULL,
  CONSTRAINT fk_climber FOREIGN KEY (climber_id) REFERENCES climber(id),
  CONSTRAINT fk_climb FOREIGN KEY (climb_id) REFERENCES climb(id)
);

-- Table: climber_climb
CREATE TABLE climber_climb (
  id bigint PRIMARY KEY,
  climber_id bigint NOT NULL,
  climb_id bigint NOT NULL,
  CONSTRAINT fk_climber FOREIGN KEY (climber_id) REFERENCES climber(id),
  CONSTRAINT fk_climb FOREIGN KEY (climb_id) REFERENCES climb(id)
);

-- Table: climber_mountain
CREATE TABLE climber_mountain (
  id bigint PRIMARY KEY,
  climber_id bigint NOT NULL,
  mountain_id varchar(100) NOT NULL,
  CONSTRAINT fk_climber FOREIGN KEY (climber_id) REFERENCES climber(id),
  CONSTRAINT fk_mountain FOREIGN KEY (mountain_id) REFERENCES mountain(name)
);

-- Table: record
CREATE TABLE record (
  id bigint PRIMARY KEY,
  climb_id bigint NOT NULL,
  climber_id bigint NOT NULL,
  name varchar NOT NULL,
  record_date date NOT NULL,
  CONSTRAINT fk_climb FOREIGN KEY (climb_id) REFERENCES climb(id),
  CONSTRAINT fk_climber FOREIGN KEY (climber_id) REFERENCES climber(id)
);
