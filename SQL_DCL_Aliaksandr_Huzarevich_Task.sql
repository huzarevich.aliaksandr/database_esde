-- Create a new user "rentaluser"
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

-- Grant the user the ability to connect to the database
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

-- Grant "rentaluser" SELECT permission for the "customer" table
GRANT SELECT ON customer TO rentaluser;

-- Create a new user group called "rental"
CREATE ROLE rental;

-- Add "rentaluser" to the "rental" group
GRANT rental TO rentaluser;

-- Grant the "rental" group INSERT and UPDATE permissions for the "rental" table
GRANT INSERT, UPDATE ON rental TO rental;

-- Revoke the "rental" group's INSERT permission for the "rental" table
REVOKE INSERT ON rental FROM rental;

-- Create a personalized role for an existing customer named Aaron Selby
CREATE ROLE client_aaron_selby;

-- Enable RLS on the "rental" and "payment" tables
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;
ALTER TABLE payment ENABLE ROW LEVEL SECURITY;

-- Create policies for the "rental" table for Aaron Selby
CREATE POLICY rental_access_policy ON rental
FOR SELECT
USING (customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Aaron' AND last_name = 'Selby'));

-- Create policies for the "payment" table for Aaron Selby
CREATE POLICY payment_access_policy ON payment
FOR SELECT
USING (customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Aaron' AND last_name = 'Selby'));

-- Grant the role SELECT on the "rental" and "payment" tables to Aaron Selby
GRANT SELECT ON rental, payment TO client_aaron_selby;
